import React, {createContext} from 'react';

export const themeConfig = {
    darkMode: {
        backgroundImage: "linear-gradient(to bottom right, $white 3%, $lightBlue 40% , $blue) !important"
    },
    lightMode: {
        backgroundColor: "navajowhite",
        backgroundImage: "none",
    },
}
export const arrowColor = {
    darkMode: {
        home:{color: "white"},
        user:{color: "white", transform: "rotate(180deg)"},
        select: {color: "white"}
    },
    lightMode: {
        home:{color: "#00307b"},
        user:{color: "#00307b", transform: "rotate(180deg)"},
        select: {color: "#00307b"}
    }
}
const ThemeContext = createContext(themeConfig['darkMode']);
export default ThemeContext;