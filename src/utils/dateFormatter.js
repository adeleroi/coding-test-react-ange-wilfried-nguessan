const dateFormatter = (dateToFormat) => {
    const date = new Date(dateToFormat);
    const months = ['janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre',
                    'octobre', 'novembre', 'décembre'];
    const formattedDate = [date.getDate(), months[date.getMonth() - 1], date.getFullYear()].join(" ");
    return formattedDate;
}

export default dateFormatter;