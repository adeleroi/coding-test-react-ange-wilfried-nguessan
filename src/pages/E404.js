import { Grid } from '@material-ui/core';
import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";

import { title } from 'utils';


class E404Page extends Component {
    render() {
        return (
            <Fragment>
                <Helmet>
                    { title('Page introuvable') }
                </Helmet>

                <div className="error-page content-wrap"
                style={{display: Grid }}>
                    <div className="error-msg">
                        <h1 style={{fontSize: "2.3vh"}}>Oups! la page est introuvable</h1>
                        <p style={{fontSize: "2.3vh"}}>Cette page ne semble pas exister ...</p>
                        <Link to="/">Retour à l'accueil</Link>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default E404Page;
