import E404Page from './E404';
import HomePage from './Home';
import UserPage from './User';


export {
    HomePage,
    UserPage,
    E404Page,

}
