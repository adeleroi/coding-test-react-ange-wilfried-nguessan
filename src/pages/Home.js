import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';
import ThemeContext from '../context/ThemeContext';
import Preference from '../components/Preference'
import { title } from 'utils';


class HomePage extends Component {
    render() {
        return (
            <ThemeContext.Consumer>
                {
                    theme => {
                    return (
                        <Fragment>
                            <Helmet>
                                { title('Page d\'accueil') }
                            </Helmet>
                            <div className="home-page content-wrap" style={theme.config}>
                                <Preference theme={theme} toggleTheme={this.props.toggleTheme}/>
                                <div className="infos-block-home">
                                    <h1 className="company-name">04h11
                                        <span className="company-name-shadow">04h11</span>
                                    </h1>
                                    <p className="company-msg">Le spécialiste de vos données.</p>
                                </div>
                                <div className="big-ball"></div>
                                <div className="small-ball"></div>
                                <Link to="/users" className="nav-arrow">
                                    <Icon style={theme.colorPalette.home}>arrow_right_alt</Icon>
                                </Link>
                            </div>
                        </Fragment>
                    )
                    }
                }
            </ThemeContext.Consumer>

    )
    }
}

export default HomePage;
