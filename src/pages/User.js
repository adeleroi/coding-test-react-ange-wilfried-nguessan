import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';
import Icon from '@material-ui/core/Icon';
import ThemeContext from '../context/ThemeContext'
import { title } from 'utils';
import User from '../components/user'
import Article from '../components/article'
import dateFormatter from '../utils/dateFormatter';
import cleanList from '../utils/cleanList';
import { userService } from 'services';


class UserPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            id: null,
            user: {occupation:null, birth:null, articles:null}
        }
        this.fetchList = this.fetchList.bind(this);
        this.fetchUser = this.fetchList.bind(this);
        this.handleSelection = this.handleSelection.bind(this);
    }

    fetchList(){
        const { id } = this.props.match.params;
        userService.list().then(x => {
            this.setState({ list: cleanList(x) });
            const userId = id ? id : x[0].id;
            this.setState({ id: userId});
            return userService.get(userId).then(x => {
                this.setState({
                    user: {
                        ...this.state.user,
                        occupation: x.occupation,
                        birth: dateFormatter(x.birthdate),
                        articles: x.articles
                }})
            })
        })
    }
    fetchUser(userId){
        userService.get(userId).then(x => {
            this.setState({
                user: {
                    ...this.state.user,
                    occupation: x.occupation,
                    birth: dateFormatter(x.birthdate),
                    articles: x.articles
            }})
        })

    }
    handleSelection(e){
        console.log(this.props)
        this.setState({ id: e.target.value});
        this.props.history.push(`/users/${e.target.value}`);
    }

    componentDidMount() {
        this.fetchList();
    }

    componentDidUpdate(prevProps, prevState){
        if(prevState.id !== this.state.id){
            console.log(prevState.id, this.state.id)
            this.fetchList(this.state.id)
        }
    }

    render() {
        if(!this.state.user.articles){
            return(
                <ThemeContext.Consumer>
                    {
                        (theme) => {
                            return (
                                <div className="user-page" style={theme.config}>
                                    <h1 className="user-loading" style={theme.colorPalette.select}>Loading...</h1>
                                </div>
                            )
                        }
                    }
                </ThemeContext.Consumer>
            )
        }
        return (
            <ThemeContext.Consumer>
                {
                    (theme) => {
                        return(
                        <Fragment>
                            <Helmet>
                                { title('Page secondaire') }
                            </Helmet>
                            <div className="user-page content-wrap" style={theme.config}>
                                <Link to="/" className="nav-arrow-user">
                                    <Icon className="user-arrow-rotate" style={theme.colorPalette.user}>
                                        arrow_right_alt
                                    </Icon>
                                </Link>
                                <div className="user-data">
                                    <div className="users-select">
                                        {/* <Icon className="drop-down-icon" style={theme.colorPalette.home}>arrow_drop_down</Icon> */}
                                        <select className="users" style={theme.colorPalette.select}
                                            onChange={(e)=> this.handleSelection(e)}
                                            value={this.state.id}>
                                            { 
                                                this.state.list.map((ob) => {
                                                    return <User usr={ob} key={ob.id}/>
                                                })
                                            }
                                        </select>
                                    </div>
                                    <div className="infos-block">
                                        <span className="user-occupation">Occupation: 
                                            <span className="user-occupation-res" style={theme.colorPalette.select}> 
                                                {this.state.user.occupation}
                                            </span>
                                        </span>
                                        <span className="user-birth-date">Date de naissance: 
                                            <span className="user-birth-date-res" style={theme.colorPalette.select}>
                                                {this.state.user.birth}
                                            </span>
                                        </span>
                                    </div>
                                </div>
                                <div className="articles-list">
                                {
                                    this.state.user.articles.map((article) => {
                                        return <Article article={article}  key={article.id}/>
                                    })
                                }
                                </div>
                            </div>
                        </Fragment>
                        )
                    }
                }
            </ThemeContext.Consumer>
        )
    }
}

export default UserPage;
