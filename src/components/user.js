import React from 'react';

const User = ({usr}) => {
    return (
        <>
            <option className="user-option" value={usr.id}>{usr.name}</option>
        </>
    )
}

export default User;