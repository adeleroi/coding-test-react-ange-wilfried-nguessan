import React from 'react';

const Preference = ({theme, toggleTheme}) => {
    return(
        <>
            <div className="theme-selector">
                <label className="switch">
                    <input type="checkbox" className="toggle-theme" value="darkMode" checked={theme.type === 'darkMode'}
                        onChange={toggleTheme}
                    />
                    <span className="slider round"></span>
                </label>
            </div>
        </>
    )
}

export default Preference;