import React from 'react';

const Article = ({article}) => {
    return (
        <>
            <div className="article">
                <h1 className="article-title">Article # {article.id}</h1>
                <p className="article-content">{article.content}</p>
            </div>
        </>
    )
}

export default Article;