import React, { Fragment } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import ThemeContext from './context/ThemeContext'
import { themeConfig, arrowColor } from './context/ThemeContext'
import {
    HomePage,
    UserPage,
    E404Page
} from 'pages';

import { history } from 'utils/history';


class AppRouter extends React.Component {
    constructor(){
        super();
        this.state = {
            theme: 'darkMode'
        }
        this.toggleTheme = this.toggleTheme.bind(this);
    }
    toggleTheme(){
        this.setState({theme: this.state.theme === "lightMode" ? 'darkMode': 'lightMode'})
    }
    render() {
        var routes = (
            <ThemeContext.Provider 
                value={
                    {type: this.state.theme, config:themeConfig[this.state.theme], colorPalette: arrowColor[this.state.theme]}
            }>
                <Switch>
                        <Route exact path="/" component={() => <HomePage toggleTheme={this.toggleTheme}/> } />
                        <Route exact path="/users" component={ (props) => <UserPage {...props} toggleTheme={this.toggleTheme}/> }/>
                        <Route exact path="/users/:id" component={ (props) => <UserPage {...props}/> } />
                        <Route component={ E404Page } />
                </Switch>
            </ThemeContext.Provider>
        )

        return (
            <div className="page-wrap">
                <Router history={ history }>
                    { routes }
                </Router>
            </div>
        )
    }
}


export default AppRouter;
